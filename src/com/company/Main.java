package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a;
        Scanner scanner = new Scanner(System.in);
        System.out.print("輸入層數:");
        a = scanner.nextInt();
        System.out.print("輸入印出字元:");
        char d = scanner.next().charAt(0);

        for(int i=1; i<=a; i++   ){

            for(int j=a-1; j>=i; j--){
                System.out.print(" ");
            }

            for(int k=1; k<=2*i-1; k++){
                System.out.print(d);
            }
            System.out.println();
        }
    }
}
